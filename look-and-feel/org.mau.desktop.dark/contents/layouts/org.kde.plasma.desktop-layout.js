loadTemplate("org.mau.desktop.appMenuBar");
loadTemplate("org.mau.desktop.dockPanel");

var appMenuPanel = new Panel;
appMenuPanel.location = "top";
appMenuPanel.height = Math.round(gridUnit * 1.5);

appMenuPanel.addWidget("org.kde.windowtitle");
appMenuPanel.addWidget("org.kde.plasma.appmenu");
appMenuPanel.addWidget("org.kde.plasma.panelspacer");
appMenuPanel.addWidget("org.kde.plasma.systemtray");
appMenuPanel.addWidget("org.kde.plasma.pager");
appMenuPanel.addWidget("org.kde.plasma.digitalclock");

var dockPanel = new Panel;
dockPanel.location = "bottom";
dockPanel.minimumLength = Math.round(gridUnit * 3);
dockPanel.maximumLength = Math.round(gridUnit * 39);
dockPanel.height = Math.round(gridUnit * 3);
dockPanel.alignment = "center";

dockPanel.addWidget("org.kde.plasma.kickerdash")
dockPanel.addWidget("org.kde.plasma.icontasks");

var desktopsArray = desktopsForActivity(currentActivity());
for (var j = 0; j < desktopsArray.length; j++) {
    desktopsArray[j].wallpaperPlugin = 'org.kde.image';
}

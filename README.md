# Mau Plasma themes and plugins

![Mau Dark global theme](look-and-feel/org.mau.desktop.dark/contents/previews/fullscreenpreview.png)

**Set of minimalistic KDE Plasma themes and plugins inspired by looks of macOS.
People migrating from GNOME with might be pleased too!**

However, it doesn't aim to imitate macOS entirely. Instead, it aims to keep the
default Plasma Breeze look while enabling visual and productivity-enhancing
elements known from macOS, like:

* Application panel on the top.
* Launcher dock on the bottom.
* Application dashboard (instead of menu).

## Global theme

It's recommended to install the "Mau Dark" global theme, which automatically
pulls all dependencies and sets up all panels.

## Dependencies

* [Window Title Aplet](https://store.kde.org/p/1274218/)
